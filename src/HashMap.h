#ifndef AISDI_MAPS_HASHMAP_H
#define AISDI_MAPS_HASHMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <algorithm>
#include <iostream>

namespace aisdi
{

template <typename KeyType, typename ValueType>
class HashMap
{
public:
  using key_type = KeyType;
  using mapped_type = ValueType;
  using value_type = std::pair<const key_type, mapped_type>;
  using size_type = std::size_t;
  using reference = value_type&;
  using const_reference = const value_type&;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;
  struct HashNode;
  using Node = HashNode;

  static const size_t TABLE_SIZE = 1031;
private:
  size_t size;
  HashNode *hash_table[TABLE_SIZE];

  size_t _hash_function(const key_type& key) const {
    return (key % TABLE_SIZE);
  }

  void _alloc_hash_table() {
    for(size_t i = 0; i < TABLE_SIZE; ++i)
      hash_table[i] = nullptr;
  }
public:
  void _clear() {
    for(size_t i = 0; i < TABLE_SIZE; ++i) {
      Node *p = hash_table[i];
      while(p != nullptr) {
        Node *tmp = p;
        p = p->next;
        delete tmp;
      }
      hash_table[i] = nullptr;
    }
    size = 0;
  }

  HashMap() : size(0)
  {
    _alloc_hash_table();
  }

  ~HashMap() {
    _clear();
  }

  HashMap(std::initializer_list<value_type> list) : size(0)
  {
    _alloc_hash_table();
    for(auto it : list)
      valueOf(it.first) = it.second;
  }

  HashMap(const HashMap& other) : size(0)
  {
    if(&other == this)
      return;
    _alloc_hash_table();
    for(auto it = other.begin();it!=other.end();++it)
      valueOf((*it).first) = (*it).second;
  }

  HashMap(HashMap&& other)
  {
    if(&other == this)
      return;
    size = other.size;

    for(size_t i = 0; i < TABLE_SIZE; ++i) {
      hash_table[i] = other.hash_table[i];
      other.hash_table[i]  = nullptr;
    }
    other.size = 0;
  }

  HashMap& operator=(const HashMap& other)
  {
    if(&other == this)
      return *this;
    _clear();
    for(auto it = other.begin();it!=other.end();++it)
      valueOf((*it).first) = (*it).second;
    return *this;
  }

  HashMap& operator=(HashMap&& other)
  {
    if(&other == this)
      return *this;
    _clear();
   size = other.size;

    for(size_t i = 0; i < TABLE_SIZE; ++i) {
      hash_table[i] = other.hash_table[i];
      other.hash_table[i]  = nullptr;

    }
    other.size = 0;
    return *this;
  }

  bool isEmpty() const
  {
    return (size == 0);
  }

  mapped_type& operator[](const key_type& key)
  {
     return (mapped_type&)valueOf(key);
  }
private:
  Node *_find_node(const key_type& key) const {
    size_t hash = _hash_function(key);
    Node *p = hash_table[hash];

    while(p != nullptr) {
      if(p->val.first == key)
        return p;
      p = p->next;
    }
    return nullptr; // not found
  }
public:
  const mapped_type& valueOf(const key_type& key) const
  {
    Node *node = _find_node(key);
    if(node == nullptr) {
      throw std::out_of_range("key not found.");
    }
    return node->val.second;
  }

  mapped_type& valueOf(const key_type& key)
  {
    size_t hash = _hash_function(key);
    Node *p = hash_table[hash];

    while(p != nullptr) {
      if(p->val.first == key) {
        return p->val.second;
      }
      p = p->next;
    }
    // create node
    p = hash_table[hash];
    Node **nn = &(hash_table[hash]);
    *nn = new Node(value_type(key,""));
    (*nn)->next = p;
    ++size;
    return (*nn)->val.second;
  }

  const_iterator find(const key_type& key) const
  {
    return const_iterator(this,_hash_function(key), _find_node(key));
  }

  iterator find(const key_type& key)
  {
    return iterator(this,_hash_function(key), _find_node(key));
  }

  void remove(const key_type& key)
  {
    if(size<1) throw std::out_of_range("remove(key): removing from empty list!");
    size_t hash = _hash_function(key);
    Node *p = hash_table[hash],
      *prev = nullptr; // parent node, if exists

    while(p != nullptr) {
      if(p->val.first == key) {
        if(prev != nullptr)
          prev->next = p->next;
        else
          hash_table[hash] = p->next;
        delete p;
        --size;
        return;
      }
      prev = p;
      p = p->next;
    }
    throw std::out_of_range("remove(key): not found!");
  }

  void remove(const const_iterator& it)
  {
    remove((*it).first);
  }

  size_type getSize() const
  {
    return size;
  }

  bool operator==(const HashMap& other) const
  {
    // first do simple check due to high time complexity of full test
    if(size != other.size)
      return false;
    // works as long as both tables use the same hash function and table size
    for(size_t i = 0; i < TABLE_SIZE; ++i)
      if((hash_table[i] == nullptr) != (other.hash_table[i] == nullptr)) // "XOR"
        return false;
    // compare all keys and values
    for(auto it = begin(); it != end(); ++it)
      if((*it) != *(other.find((*it).first)))
        return false;
    return true;
  }

  bool operator!=(const HashMap& other) const
  {
    return !(*this == other);
  }

  iterator begin()
  {
    // find the first item or nullptr if list empty
    Node *p = nullptr;
    size_t i = 0;
    for(; i < TABLE_SIZE; ++i)
      if(hash_table[i] != nullptr) {
        p = hash_table[i];
        break;
      }
    iterator result(this,i,p);
    return result;
  }

  iterator end()
  {
    // node = nullptr means end
    iterator result(this,0,nullptr);
    return result;
  }

  const_iterator cbegin() const
  {
    // find the first item or nullptr if list empty
    Node *p = nullptr;
    size_t i = 0;
    for(; i < TABLE_SIZE; ++i)
      if(hash_table[i]) {
        p = hash_table[i];
        break;
      }
    const_iterator result(this,i,p);
    return result;
  }

  const_iterator cend() const
  {
    // node = nullptr means end
    const_iterator result(this,0,nullptr);
    return result;
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }
};

template <typename KeyType, typename ValueType>
class HashMap<KeyType, ValueType>::ConstIterator
{
public:
  using reference = typename HashMap::const_reference;
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename HashMap::value_type;
  using pointer = const typename HashMap::value_type*;

  const HashMap* map;
  size_t index;
  Node* node;

  explicit ConstIterator() { map = nullptr; index = 0; node = nullptr;}

  ConstIterator(HashMap const*  m, size_t i, Node* n) : map(m), index(i), node(n)
  { }

  ConstIterator(const ConstIterator& other)
  {
    map = other.map;
    node = other.node;
    index = other.index;
  }

  ConstIterator& operator++()
  {
    if(node == nullptr)
      throw std::out_of_range("iterator++ after the end!");
    if(node->next) {
      node = node->next;
    } else {
      index++;
      for(;index < HashMap::TABLE_SIZE; index++) {
        if(map->hash_table[index] != nullptr) {
          node = map->hash_table[index];
          return *this;
        }
      }
      node = nullptr;
    }
    return *this;
  }

  ConstIterator operator++(int)
  {
    auto result = *this;
    operator++();
    return result;
  }

  // very slow, but actually won't be used
  ConstIterator& operator--()
  {
    if(node == nullptr) { // iterator is at end
      if(map->size == 0)
        throw std::out_of_range("iterator-- on empty list!");
      index = HashMap::TABLE_SIZE;
    } else // check if could just move within the list
    if(node->next) {
      Node *prev;
      prev = node = map->hash_table[index];
      if(node != prev) // find within the same list
        while(node->next) {
          if(node->next == prev)
            return *this; // found
          node = node->next;
        }
    }
    // go to the last element in the previous hash
    while(index-- > 0) {
      if((node = map->hash_table[index]) != nullptr) {
        while(node->next)
          node = node->next;
        break;
      }
    }
    // if not found, iterator is at nullptr -> end
    return *this;
  }

  ConstIterator operator--(int)
  {
    auto result = *this;
    operator--();
    return result;
  }

  reference operator*() const
  {
    if(node == nullptr)
      throw std::out_of_range("dereferencing an invalid iterator!");
    return node->val;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  bool operator==(const ConstIterator& other) const
  {
    return (map == other.map && node == other.node);
  }

  bool operator!=(const ConstIterator& other) const
  {
    return !(*this == other);
  }
};

template <typename KeyType, typename ValueType>
class HashMap<KeyType, ValueType>::Iterator : public HashMap<KeyType, ValueType>::ConstIterator
{
public:
  using reference = typename HashMap::reference;
  using pointer = typename HashMap::value_type*;

  explicit Iterator() : ConstIterator() {
  }

  Iterator(HashMap *m, size_t i, Node* n) : ConstIterator(m,i,n)
  {}

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

template <typename KeyType, typename ValueType>
struct HashMap<KeyType, ValueType>::HashNode
{
  HashNode(value_type v) : val(v){
    next = nullptr;
  }
  value_type val;
  HashNode *next, *prev;
};

}

#endif /* AISDI_MAPS_HASHMAP_H */
