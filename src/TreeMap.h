#ifndef AISDI_MAPS_TREEMAP_H
#define AISDI_MAPS_TREEMAP_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <utility>
#include <iostream>

namespace aisdi
{

template <typename KeyType, typename ValueType>
class TreeMap
{
public:
  using key_type = KeyType;
  using mapped_type = ValueType;
  using value_type = std::pair<const key_type, mapped_type>;
  using size_type = std::size_t;
  using reference = value_type&;
  using const_reference = const value_type&;

  class ConstIterator;
  class Iterator;
  struct TNode;
  using Node = TNode;
  using iterator = Iterator;
  using const_iterator = ConstIterator;
private:
  size_type size; // number of elements
  Node *root; // root of the tree structure
public:
  TreeMap() : size(0), root(nullptr)
  {
  }

  ~TreeMap() {
    _clear(root);
  }

  TreeMap(std::initializer_list<value_type> list) : size(0), root(nullptr)
  {
    for(auto it=list.begin();it!=list.end();++it)
      _assign((*it).first, (*it).second);
  }

  TreeMap(const TreeMap& other) : size(0), root(nullptr)
  {
    if(&other == this) throw std::runtime_error("self-assignment in copy constructor!");
	// copy the other list
    for(auto it = other.begin();it != other.end();++it)
      _assign((*it).first, (*it).second);
  }

  TreeMap(TreeMap&& other)
  {
    if(&other == this) throw std::runtime_error("self-assignment in move constructor!");
    root = other.root;
    other.root = nullptr;
    size = other.size;
    other.size = 0;
  }

  // clear the list
  void _clear(Node *&node) {
    if(node == nullptr)
      return;
    _clear(node->left);
    _clear(node->right);
    delete node;
    node = nullptr;
    size = 0;
  }
  void _clear() {
	  _clear(root);
  }

  TreeMap& operator=(const TreeMap& other)
  {
    if(&other == this) return *this; // nothing happens
    _clear(root);
    // copy the other list
    for(auto it = other.begin(); it != other.end(); ++it)
      (*this)[(*it).first] = (*it).second;
    return *this;
  }

  TreeMap& operator=(TreeMap&& other)
  {
    if(&other == this) return *this;
    _clear(root);
    root = other.root;
    other.root = nullptr;
    size = other.size;
    other.size = 0;
    return *this;
  }

  bool isEmpty() const
  {
    return size == 0;
  }

  void _assign(const key_type& key, const mapped_type& val) {
    Node* n = _get(key);
    n->val.second = val;
  }

  // get a node with key value key
  // create if needed
  Node *_get(const key_type& key) {
    Node *cn = root, *last = nullptr;
    Node**ins = &root;

    size_t bare_left = 0, bare_right = 0;

    while(cn != nullptr) {

        if(0 && bare_left > 4 && cn->parent->parent != root) {
          bare_left = bare_right = 0;
          Node *opty = cn;
          if(opty->parent->parent == nullptr) std::cout << "no base( 0 )!!!!"<<std::endl;
          Node *base = opty->parent->parent->parent;
          if(base==nullptr) std::cout << "no base!!!!"<<std::endl;
          Node *sibling = opty->parent->parent;
          Node *parent = opty->parent;
          //std::cout <<"optimizing "<<base->val.first<<","<<sibling->val.first<<","<<opty->parent->val.first<<","<<opty->val.first<<std::endl;
          sibling->parent = parent;
          sibling->left = nullptr;
          sibling->right = nullptr;
          parent->left = sibling;

          parent->parent = base;

         base->right = parent;


          bool test0 = (opty->parent->parent == base);
         // bool test1 = (base->right == opty->parent);
         // bool test2 = (base->right->right == opty);
          bool test3 = (opty->parent->left == sibling);
          bool test4 = (sibling->parent == opty->parent);
          bool test5 = (sibling->right == nullptr && sibling->left == nullptr);
          bool test6 = (parent->right == opty);
        size_t sum = (0+test0+test3+test4+test5+test6);
        if(sum != 5) {std::cout << "TEST NIE PRZESZED� :(\n";
        if(test0 == false) std::cout << "[ 0 ]";
        // if(test1 == false) std::cout << "[ 1 ]";
        //if(test2 == false) std::cout << "[ 2 ]";
        if(test3 == false) std::cout << "[ 3 ]";
        if(test4 == false) std::cout << "[ 4 ]";
        if(test5 == false) std::cout << "[ 5 ]";
        if(test6 == false) std::cout << "[ 6 ]";
        }
        // std::cout << "r�wnowa�enie go�ej lewej"<<std::endl;
          bare_right = bare_left = 0;
          continue;
      }
      if(bare_right > 4) {
        //std::cout << "r�wnowa�enie go�ej prawej"<<std::endl;
        bare_right = bare_left = 0;
      }

      if(cn->val.first == key)
        return cn;

      last = cn;



      if(cn->left == nullptr) {
        bare_left++;
        bare_right = 0;
      }
      if(cn->right == nullptr) {
        bare_right++;
        bare_left = 0;
      }


      if(cn->val.first > key) {
        ins = &(cn->left);
        cn = cn->left;
      } else {
        ins = &(cn->right);
        cn = cn->right;
      }
    }
    // create a new node
    *ins = new Node(value_type(key,""));
    (*ins)->parent = last;
    //if((*ins)->parent == root) std::cout << " SYN ROOT :"<<(((*ins)->parent->parent != nullptr) ? 1 : 0) << std::endl;
    ++size;
    return *ins;
  }

  mapped_type& operator[](const key_type& key)
  {
    return valueOf(key);
  }

  const mapped_type& valueOf(const key_type& key) const
  {
    Node *node = _find_node(key);
    if(node == nullptr)
      throw std::out_of_range("operator[]: key not found.");
    return node->val.second;
  }

  inline mapped_type& valueOf(const key_type& key)
  {
    Node* n = _get(key);
    return n->val.second;
  }

  // find a node with key value key or return end for missing key
  Node *_find_node(const key_type& key) const {
    if(size == 0)
      return nullptr;
    Node *cn = root;
    while(cn != nullptr) {
      if(cn->val.first == key)
        return cn;
      if(cn->val.first > key)
        cn = cn->left;
      else
        cn = cn->right;
    }
	  return nullptr;
  }

  const_iterator find(const key_type& key) const
  {
    return const_iterator(_find_node(key));
  }

  iterator find(const key_type& key)
  {
    return iterator(_find_node(key));
  }

  void remove(const key_type& key)
  {
    remove(find(key));
  }

  size_t _count(Node *node) {
    if(node == nullptr) return 0;
    return _count(node->left) + _count(node->right) + 1;
  }

  void remove(const const_iterator& it)
  {
    if(it == end() || root == nullptr || it.node == nullptr)
      throw std::out_of_range("trying to remove from an empty tree/by an invalid iterator.");
    Node *node = it.node;
    Node *del = node;
    //std::cout<<"\nremove "<< node->val.first<<" l="<<getSize()<<" count:"<<_count(root);
    // first, check if there is the only one element
    if(size > 1) {
      Node **parentLink = node->parentLink();
      // no children - just delete the leaf
      if(node->left == nullptr && node->right == nullptr) {
        //if(parentLink != nullptr) // if size > 0 and leaf, always true
           *parentLink = nullptr;
          // std::cout << "<A>" << std::endl;
      } else if(node->left == nullptr || node->right == nullptr) {
      // one child node - replace the deleted node with the child
        Node *child = (node->left == nullptr) ? node->right : node->left;
        if(parentLink != nullptr)
          *parentLink = child;
        else root = child;
        child->parent = node->parent;
       //  std::cout << "<B>" << std::endl;
      } else {
      // both children - replace the removed node with its successor
      // next - successor, prec - reference to successor
        Node *next = node->right, **nextLink = &(node->right);
      // find the successor
        while(next->left != nullptr) {
          nextLink = &next->left;
          next = next->left;
        }
        // modification of the right branch
        (*nextLink) = next->right; // successor can only have right children
        if(next->right != nullptr)
          next->right->parent = next->parent;
        next->right = node->right;

        if(next->right != nullptr)
          next->right->parent = next;
        if(parentLink != nullptr)
          *parentLink = next;
        else
          root = next;
        // left and parent as in deleted
        next->left = node->left;
        next->left->parent = next;
        next->parent = node->parent;
      }
    }
    --size;
    if(del != nullptr)
      delete del;
    else throw std::runtime_error("nothing to delete?!");
    if(size == 0)
      root = nullptr;
    return;
  }

  size_type getSize() const
  {
    return size;
  }

  bool operator==(const TreeMap& other) const
  {
    if(size != other.size)
		  return false;

    for(auto it = begin();it != end();++it) {
      /* a key doesn't appear in other tree */
      if(other.find((*it).first)==other.end())
          return false;
      /* values on the same key don't match*/
      if(valueOf((*it).first) != other.valueOf((*it).first))
        return false;
    }
    return true;
  }

  bool operator!=(const TreeMap& other) const
  {
    return !(*this == other);
  }

  iterator begin()
  {
    Node *node = root;
    // if the list is empty, iterator at nullptr
    if(node != nullptr)
      while(node->left != nullptr)
        node = node->left;
    iterator it(node);
    it.size = size;
    it.root = root;
    return it;
  }

  iterator end()
  {
    iterator it(nullptr);
    it.size = size;
    it.root = root;
    return it;
  }

  const_iterator cbegin() const
  {
    Node *node = root;
    if(node != nullptr)
      while(node->left != nullptr)
        node = node->left;
    const_iterator it(node);
    it.size = size;
    it.root = root;
    return it;
  }

  const_iterator cend() const
  {
    const_iterator it(nullptr);
    it.size = size;
    it.root = root;
    return it;
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }
};

template <typename KeyType, typename ValueType>
class TreeMap<KeyType, ValueType>::ConstIterator
{
public:
  using reference = typename TreeMap::const_reference;
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename TreeMap::value_type;
  using pointer = const typename TreeMap::value_type*;

  Node *node, *root;
  size_t size = 0;

  explicit ConstIterator()
  {
    node = nullptr;
    size = 0;
  }

  ConstIterator(Node *n) : node(n) {
	  size = 0;
  }

  ConstIterator(const ConstIterator& other)
  {
    node = other.node;
    size = other.size;
  }

  ConstIterator& operator++()
  {
    if(node == nullptr || size == 0) throw std::out_of_range("iterator++/--");

    Node* next = node;
    // get the successor
    // no successor - point at the end
    if(next->right == nullptr && next->parent == nullptr) {
      node = nullptr;
      return *this;
    }

    if(node->right != nullptr) {
      next = node->right;
      while(next->left)
        next = next->left;
    } else {
     // no higher children
      while(next != nullptr && next->val.first <= node->val.first)
        next = next->parent;
    }
    node = next;
    return *this;
  }

  ConstIterator operator++(int)
  {
    auto result = *this;
    operator++();
    return result;
  }

  ConstIterator& operator--()
  {
    Node* next = root;
    // get the closest successor
    if(node == nullptr && size < 1) throw std::out_of_range("iterator++/--");
    // get the last element if decrementing "end()" value
    if(node == nullptr) {
       while((next->right) != nullptr)
           next = next->right;
    } else {
      if(node->left != nullptr) {
        next = node->left;
        while(next->right)
        next = next->right;
      } else {
        // no lower children
        while((next = next->parent) != nullptr && next->val.first >= node->val.first)
          ;
      }
    }
    node = next;
    return *this;
  }

  ConstIterator operator--(int)
  {
    auto result = *this;
    operator--();
    return result;
  }

  reference operator*() const
  {
	if(node == nullptr)
		throw std::out_of_range("invalid iterator.");
    return node->val;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  bool operator==(const ConstIterator& other) const
  {
    return node == other.node;
  }

  bool operator!=(const ConstIterator& other) const
  {
    return !(*this == other);
  }
};

template <typename KeyType, typename ValueType>
class TreeMap<KeyType, ValueType>::Iterator : public TreeMap<KeyType, ValueType>::ConstIterator
{
public:
  using reference = typename TreeMap::reference;
  using pointer = typename TreeMap::value_type*;

  explicit Iterator() : ConstIterator()
  {
  }

  Iterator(Node *n) : ConstIterator( n) {
  }

  Iterator(const ConstIterator& other)
    : ConstIterator(other)
  {}

  Iterator& operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator& operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  pointer operator->() const
  {
    return &this->operator*();
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};

template <typename KeyType, typename ValueType>
struct TreeMap<KeyType, ValueType>::TNode
{
  TNode *left, *right, *parent;
  value_type val;

  TNode(value_type vt) : val(vt) {
    left = right = parent = nullptr;
  }
  Node **parentLink() {
    if(parent == nullptr)
      return nullptr;
    if(parent->left == this)
      return &(parent->left);
    else
      return &(parent->right);
  }
};

}

#endif /* AISDI_MAPS_MAP_H */
