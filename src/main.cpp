#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include "../src/HashMap.h"
#include "../src/TreeMap.h"

#include <ctime>

using namespace std;
using hashMap = aisdi::HashMap<int, std::string>;
using treeMap = aisdi::TreeMap<int, std::string>;

template <typename T>
void performTests(int repeats, int collectionSize, vector<int>& keys) {
  T map;
  clock_t test_time;
  clock_t begin_time = clock();
  // insert - sorted keys
  for(int j = 0; j < repeats; ++j) {
	   map._clear();
	  for(int i = 0; i < collectionSize; ++i) {
		  map[i] = "string";
	  }
  }

  for(int i = 0; i < collectionSize; ++i) {
      auto it = map.find(i);
      if((*it).first != i)
        cout << "not that! "<<(*it).first<<" / "<<i<<endl;
  }
  std::cout << "Insert sorted keys: " << float(clock () - begin_time) / CLOCKS_PER_SEC << "\n";

  // delete - sorted keys
  test_time = 0;
  for(int j = 0; j < repeats; ++j) {
	  for(int i = 0; i < collectionSize; ++i) {
		  map[i] = "string";
	  }
	  begin_time = clock();
	  std::random_shuffle(keys.begin(), keys.end());
	  for(int i = 0; i < collectionSize; ++i) {
		  map.remove(i);
	  }
	  test_time += (clock () - begin_time);
  }
  std::cout << "Delete sorted keys: " << float( test_time ) /  CLOCKS_PER_SEC << "\n";


  // insert - random keys
  begin_time = clock();
  for(int j = 0; j < repeats; ++j) {
	  map._clear();
	  std::random_shuffle(keys.begin(), keys.end());
	  for(int i = 0; i < collectionSize; ++i) {
		  map[keys[i]] = "string";
	  }
  }

  std::cout << "Insert random keys: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << "\n";

  // insert - same hash
  begin_time = clock();
  for(int j = 0; j < repeats; ++j) {
	  map._clear();
	  size_t k = 0;
	  std::random_shuffle(keys.begin(), keys.end());
	  for(int i = 0; i < collectionSize; ++i) {
		  map[k] = "string";
		  k += aisdi::HashMap<int,std::string>::TABLE_SIZE;
	  }
  }

  std::cout << "Insert same hash: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << "\n";

  // find - random keys
  begin_time = clock();
  map._clear();
  for(int i = 0; i < collectionSize; ++i) {
	  map[keys[i]] = "string";
  }
  for(int j = 0; j < repeats; ++j) {

	  for(int i = 0; i < collectionSize; ++i) {
		  auto it = map.find(keys[i]);
		  if(it == map.end()) cout << keys[i]<< "not found!\n";
		  if((*it).first != keys[i])
        cout << "not that! "<<(*it).first<<" / "<<keys[i];
	  }

  }
  std::cout << "Find random keys: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << "\n";

  // delete random keys
  test_time = 0;
  for(int j = 0; j < repeats; ++j) {
	  for(int i = 0; i < collectionSize; ++i) {
		  map[keys[i]] = "string";
	  }
	  begin_time = clock();
	  std::random_shuffle(keys.begin(), keys.end());
	  for(size_t i = 0; i < collectionSize; ++i) {
		  map.remove(keys[i]);
	  }
	  test_time += (clock () - begin_time);
  }
  std::cout << "Delete random keys: " << float( test_time ) /  CLOCKS_PER_SEC << "\n";

}

int main(int argc, char *argv[])
{
  const std::size_t repeatCount = argc > 1 ? std::atoll(argv[1]) : 1;
  const std::size_t collectionSize = argc > 2 ? std::atoll(argv[2]) : 50000;

  vector<int> keys;
  for(size_t i = 0; i < collectionSize; ++i)
	  keys.push_back((int)i);
  std::random_shuffle(keys.begin(), keys.end());

  cout << "HashMap:\n";
  performTests<hashMap>((int)repeatCount, (int)collectionSize, keys);
  cout << "\nTreeMap:\n";
  performTests<treeMap>((int)repeatCount, (int)collectionSize, keys);
  cout << "\nTests finished.\n";
  return 0;
}
